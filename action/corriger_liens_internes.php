<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function action_corriger_liens_internes_dist() {
}

/**
 * Remplacer un art123 par art456 si
 * id_article=456 et id_source=123
 *
 * @param int $id_article
 * @param int $id_secteur
 * @param array $champs
 * @return array
 */
function convertisseur_corriger_liens_internes($id_article, $id_secteur, $champs = ['texte']) {
	include_spip('base/abstract_sql');

	// retrocompat ancienne signature
	if (is_string($champs)) {
		$champs = [$champs];
	}

	$article = sql_fetsel(implode(',', $champs), 'spip_articles', 'id_article=' . intval($id_article));
	if (empty($article)) {
		spip_log("convertisseur_corriger_liens_internes: article #$id_article inexistant", 'convertisseur' . _LOG_ERREUR);
		return null;
	}

	$set = [];

	// recaler des liens [->123456] ?
	include_spip("src/Texte/Collecteur/Liens");
	foreach ($article as $champ => $texte) {
		$collecteurLiens = new Spip\Texte\Collecteur\Liens();
		$liens = $collecteurLiens->collecter($texte);
		if (!empty($liens)) {
			$liens = array_reverse($liens);
			foreach ($liens as $l) {
				if (preg_match('/^(?:art)?([0-9]+)$/', trim($l['href']), $m)) {
					$id_source = $m[1];
					// trouver l'article dont l'id_source est $l[4] dans le secteur
					if ($id_dest = sql_getfetsel('id_article', 'spip_articles', "id_source=".intval($id_source) . " and id_secteur=".intval($id_secteur))) {
						$href = str_replace($id_source, $id_dest, $l['href']) ;
						$lien_corrige = str_replace("->" . $l['href'], "->$href", $l['raw']);

						if ($lien_corrige !== $l['raw']) {
							spip_log("$id_article ($champ) : ".$l['raw']." => $lien_corrige", 'correction_liens_internes' . _LOG_DEBUG);

							// maj le texte
							if (!isset($set[$champ])) {
								$set[$champ] = $texte;
							}
							$set[$champ] = substr_replace($set[$champ], $lien_corrige, $l['pos'], $l['length']);
						}
					} else {
						spip_log("$id_article ($champ) : dans " . $l['raw'] . ', lien vers ' . $id_source . ' non trouvé', 'correction_liens_internes' . _LOG_ERREUR);
					}
				}
			}
		}
	}
	if (!empty($set)) {
		sql_updateq('spip_articles', $set, "id_article=" . intval($id_article));
	}
	return $set;
}
