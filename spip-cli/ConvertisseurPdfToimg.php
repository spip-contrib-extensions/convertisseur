<?php

/***
 *
 * Convertir des fichiers PDF format JPG avec imagemagick convert.
 * Installer Image Magick pour que cela fonctionne. (brew install imagemagick sous Mac)
 *
 * // Info bonus, si on veut extraire le texte d'un PDF, on peut utiliser la commande pdftotext de poppler (brew install poppler sous Mac)
 *
 * // On l'appelle de plusieurs facons
 * 1) mode un PDF (une ou plusieurs pages) spip pdf2img path/to/fichier.pdf
 * 2) un pdf multipages avec format de pages particulier en destination : spip pdf2img -d path/to/pdf_%02d.jpg path/to/fichier.pdf
 * 3) traitements par lot des pdf (une page) d'un repertoire source vers un repertoire dest : spip pdf2img -s path -d path
 *
 * On peut aussi rogner en haut ou sur les cotés avec l'option shave : -c XxY. (exemples : -c 40x40 ou bien -c x40, ou encore -c 40x)
 *
 * On obtient des jpg de 1500 px de large en sortie.
 *
 * // a fusionner avec optimg ??
 */

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConvertisseurPdfToimg extends Command {
	protected function configure() {
		$this
			->setName('convertisseur:pdf:toimg')
			->setDescription('Conversion d\'un PDF en image(s).')
			->setAliases([
				'pdf2img' // abbréviation commune pour ca
			])
			->addArgument(
				'pdf',
				InputArgument::OPTIONAL,
				'PDF à convertir en image(s) de 1500 px de large.',
				''
			)
			->addOption(
				'source',
				's',
				InputOption::VALUE_OPTIONAL,
				'Répertoire source avec des PDF dedans pour un traitement par lot',
				''
			)
			->addOption(
				'dest',
				'd',
				InputOption::VALUE_OPTIONAL,
				'Répertoire de destination d\'un traitement par lot exemple -d /path/to/jpg, ou format pour un pdf multipages, exemple -d path/to/pdf_%02d.jpg',
				''
			)
			->addOption(
				'shave',
				'c',
				InputOption::VALUE_OPTIONAL,
				'Rogner en hauteur ou largeur avec -c XxY. (exemples : -c 40x40 ou bien -c x40, ou encore -c 40x)',
				'0'
			);
	}

	// prevoir une option crop du genre : convert -verbose -colorspace RGB -interlace none -density 300 -resize 2000 -background white -alpha remove -crop 1734x2574+0+0 +repage

	protected function execute(InputInterface $input, OutputInterface $output) {

		include_spip('iterateur/data');

		$source = $input->getOption('source');
		$dest = $input->getOption('dest');
		$shave = $input->getOption('shave');
		$pdf = $input->getArgument('pdf');

		chdir(_ROOT_RACINE);

		if (!function_exists('passthru')) {
			$output->writeln('<error>Votre installation de PHP doit pouvoir exécuter des commandes externes avec la fonction passthru().</error>');
			return self::FAILURE;
		}

		$output->writeln("<info>C'est parti pour une petite conversion de PDF en images !</info>");

		// Répertoire dest, ou arrivent les fichiers jpg
		// attention au cas PDF complet : jpg/2016-08/LMDES_2016-08_%02d.jpg
		if (preg_match('/\.jpg$/', $dest)) {
			$dirdest = dirname($dest);
		} elseif ($dest == '' and $pdf) {
			$dirdest = dirname($pdf);
		} else {
			$dirdest = $dest;
		}

		if (!is_dir($dirdest)) {
			$output->writeln("<error>Créer le répertoire $dirdest où exporter les fichiers de $source. spip pdf2img -d `repertoire` </error>");
			return self::FAILURE;
		}

		# Conversion d'un pdf ou plusieurs ?
		$fichiers_pdf = [];
		if ($pdf) {
			$fichiers_pdf = [$pdf];
		} else {
			$fichiers_pdf = glob("$source/*.pdf");
		}

		$nb_pdfs = count($fichiers_pdf);
		if (!$nb_pdfs) {
			$this->io->care("Rien à faire, aucun PDF dans $source");
			return self::FAILURE;
		}

		if ($nb_pdfs > 1) {
			$output->writeln("<info>$nb_pdfs PDFs à convertir dans $source/</info>");
		}
		foreach ($fichiers_pdf as $fichier_pdf) {
			$infos = self::pdfInfo($fichier_pdf);
			if (!$infos or empty($infos['pages'])) {
				$this->io->fail("Echec lecture des informations de $fichier_pdf");
			}
			else {
				$nbpages = $infos['pages'];
				$output->writeln("<info>Conversion de $fichier_pdf ($nbpages pages) dans $dirdest ($dest)</info>");
				$result_code = self::pdf2img($output, $fichier_pdf, $dest, $infos['pages'], $shave);
				if ($result_code) {
					$this->io->error("Echec de la conversion code $result_code");
					return self::FAILURE;
				}
			}
		}

		return self::SUCCESS;
	}


	protected static function pdf2img(OutputInterface $output, $fichier_pdf, $dest, $nb_pages, $shave) {
		$before = [];
		if (preg_match(',\.\w+$,', $dest)) {
			$dir_dest = dirname($dest) . '/';
			$fichier_dest = $dest;
			// noter ici les fichiers jpg existant avant et leur timestamp
			$deja = glob($dir_dest . '*.jpg');
			foreach ($deja as $fichier_jpg) {
				$before[$fichier_jpg] = filemtime($fichier_jpg);
			}
			$expected = false;
		}
		else {
			$dir_dest = rtrim($dest) . '/';
			$fichier_dest = $dir_dest . basename($fichier_pdf, '.pdf') . '.jpg';
			$expected = $fichier_dest;
		}
		if (!is_dir($dir_dest)) {
			throw new \Exception("Repertoire $dir_dest inexistant");
		}


		$output->writeln("Conversion (shave $shave) de $fichier_pdf dans $fichier_dest");

		// executer la commande : on delegue a une fonction surchargeable
		$exec_pdf2img = charger_fonction('exec_pdf2img', 'convertisseur');
		$result_code = $exec_pdf2img($fichier_pdf, $fichier_dest, $shave);

		if (!$result_code) {
			clearstatcache();
			// verifier que les fichiers produits ne sont pas vides
			$after = ($expected ? [$expected] : glob($dir_dest . '*.jpg'));
			$fichiers_generes = [];
			include_spip('inc/filtres');
			foreach ($after as $fichier_jpg) {
				if (!isset($before[$fichier_jpg]) or filemtime($fichier_jpg) > $before[$fichier_jpg]) {
					// c'est un fichier produit
					$filesize = @filesize($fichier_jpg);
					if (!file_exists($fichier_jpg) or !$filesize) {
						$result_code = 127;
						$output->writeln("<error>Fichier $fichier_jpg vide</error>");
						@unlink($fichier_jpg);
					}
					else {
						$fichiers_generes[$fichier_jpg] = "$fichier_jpg (" . taille_en_octets($filesize) . ')';
					}
				}
			}
			$output->writeln('<info>Fichiers générés : ' . implode(', ', $fichiers_generes) . '</info>');
		}

		return $result_code;
	}

	/**
	 * Lire les informations d'un PDF via pdfinfo
	 * @param string $fichier_pdf
	 * @return array|false
	 * @throws Exception
	 */
	protected static function pdfInfo($fichier_pdf) {
		$infos = [];
		$command = 'pdfinfo ' . escapeshellarg($fichier_pdf);
		$result = shell_exec($command);
		if (!$result) {
			include_spip('inc/convertisseur');
			convertisseur_tester_binaire('pdfinfo');
			return false;
		}

		$lignes = explode("\n", $result);
		foreach ($lignes as $ligne) {
			if ($ligne and str_contains($ligne, ':')) {
				$ligne = explode(':', $ligne, 2);
				$k = trim(reset($ligne));
				$k = strtolower($k);
				$k = str_replace(' ', '_', $k);
				$infos[$k] = trim(end($ligne));
			}
		}

		return $infos;
	}
}
