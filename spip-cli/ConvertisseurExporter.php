<?php

/***

Exporter la table spip_articles en format txt

Lancer la commande spip-cli : spip export -d `repertoire destination`

Les fichiers txts sont placés dans le repertoire `repertoire destination` sur le disque dur.

Si un repertoire git est trouvé dans /dest alors on prend le repertoire. todo

Voir aussi fichiersImporter.

*/

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class ConvertisseurExporter extends Command {
	protected function configure() {
		$this
			->setName('convertisseur:exporter')
			->setDescription('Exporter la table spip_articles (ou autre) au format SPIP txt.')
			->setAliases([
				'export'
			])
			->addOption(
				'source',
				's',
				InputOption::VALUE_OPTIONAL,
				'Table à exporter',
				'spip_articles'
			)
			->addOption(
				'dest',
				'd',
				InputOption::VALUE_OPTIONAL,
				'Répertoire où exporter au format texte',
				'spip_articles'
			)
			->addOption(
				'branche',
				'b',
				InputOption::VALUE_OPTIONAL,
				'branche à exporter (id_secteur ou id_rubrique)',
				'0'
			)
			->addOption(
				'statuts',
				't',
				InputOption::VALUE_OPTIONAL,
				'statuts des articles a exporter (séparé par une virgule)',
				'prop,prepa,publie'
			)
			->addOption(
				'modif',
				'm',
				InputOption::VALUE_OPTIONAL,
				'date_modif après laquelle exporter',
				''
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('iterateur/data');

		$source = $input->getOption('source') ;
		$dest = $input->getOption('dest') ;
		$branche = $input->getOption('branche') ;
		$date_modif = $input->getOption('modif') ;
		$statuts = explode(',', $input->getOption('statuts'));
		$statuts = array_map('trim', $statuts);
		$statuts = array_filter($statuts);

		// Secteur ou rubrique à exporter.
		if (!$branche or !intval($branche)) {
			$this->io->error("Préciser l'id du secteur ou de la rubrique à exporter. spip export -b 123");
			return self::FAILURE;
		}

		// demande t'on un secteur ou une rubrique ?
		$parent = sql_getfetsel('id_parent', 'spip_rubriques', 'id_rubrique=' . intval($branche));

		include_spip('inc/rubriques');
		$where = [];
		if($parent == 0) {
			$critere_export = 'id_secteur=' . intval($branche) ;
		} else{
			// y'a t'il des sous rubriques ?
			$sous_rubriques = calcul_branche_in($branche);
			if($sous_rubriques){
				$critere_export = sql_in('id_rubrique', $sous_rubriques) ;
			}
			else {
				$critere_export = 'id_rubrique=' . intval($branche) ;
			}
		}
		$where[] = $critere_export;

		if($date_modif) {
			$where[] = 'date_modif > ' . sql_quote($date_modif);
		}

		$where[] = sql_in('statut', $statuts);

		// Répertoire dest, ou arrivent les fichiers txt.
		if(!is_dir($dest)){
			$this->io->error("Préciser le répertoire où exporter les fichiers de $source au format txt. spip export -d `repertoire`");
			return self::FAILURE;
		}

		if (!function_exists('passthru')){
			$this->io->error("Votre installation de PHP doit pouvoir exécuter des commandes externes avec la fonction passthru().");
			return self::FAILURE;
		}

		// Si c'est bon on continue
		// chopper les articles en sql.
		$res = sql_select('*', 'spip_articles', $where, '', 'date_redac ASC');
		$nb_exports = sql_count($res);
		if($nb_exports > 0){
			// start and displays the progress bar
			$progress = new ProgressBar($output, sql_count($res));
			$progress->setBarWidth(100);
			$progress->setRedrawFrequency(1);
			$progress->setMessage(" Export de `spip_articles` branche $branche en cours dans $dest ... ", 'message');
			$progress->start();

			include_spip('action/exporter');
			while($row = sql_fetch($res)){

				$progress->setMessage('', 'motscles');
				$progress->setMessage('', 'docs');
				$progress->setMessage('', 'auteurs');

				if($e=exporter_article($row,$dest)){
					// Si tout s'est bien passé, on avance la barre
					$progress->setMessage($e['docs_m'], 'docs');
					$progress->setMessage($e['motscles_m'], 'motscles');
					$progress->setMessage($e['auteurs_m'], 'auteurs');
					$nom_fichier_m = substr($e['nom_fichier'], 0, 100) ;
					$progress->setMessage($nom_fichier_m, 'filename');
					$progress->setFormat("<fg=white;bg=blue>%message%</>\n" . '%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%' . "\n %auteurs% %motscles% \n %filename% \n\n");
					$progress->advance();
				} else{
					$this->io->error("échec de l'export de l'article #".$row['id_article']);
					return self::FAILURE;
				}
			}

			// ensure that the progress bar is at 100%
			$progress->finish();
			$this->io->success("$nb_exports exports terminés");

		} else {
			$this->io->fail("Rien à exporter dans la branche $branche depuis $date_modif");
			return self::SUCCESS;
		}

		return self::SUCCESS;
	}
}
