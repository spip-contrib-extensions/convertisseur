# lignes de commandes

**spip optimg -h**

```
Usage:
  convertisseur:images:optimiser [options] [--] [<image>]
  optimiser_images
  optimg

Arguments:
  image                            Image à optimiser.

Options:
  -s, --source[=SOURCE]            Répertoire source. Optimiser toutes les images d'un répertoire. Exemple : `spip optimg -s IMG`. [default: "0"]
  -d, --dest[=DEST]                Répertoire de destination. Exemple : `spip optimg -d mon_repertoire mon_image.jpg`. Si ce répertoire n'est pas précisé, on écrase l'image avec sa version optimisée. [default: "0"]
  -r, --resize[=RESIZE]            Redimensionner la largeur à n px, en conservant les proportions pour calculer la hauteur. Exemple : `spip optimg -r 900 mon_image.jpg` [default: "0"]
  -c, --compression[=COMPRESSION]  Compresser les images à 80%. Exemple : `spip optimg -c 80 mon_image.jpg` [default: "0"]
```

**spip pdf2img -h**

```
Usage:
  convertisseur:pdf:toimg [options] [--] [<pdf>]
  pdf2img

Arguments:
  pdf                    PDF à convertir en image(s) de 1500 px de large. [default: ""]

Options:
  -s, --source[=SOURCE]  Répertoire source avec des PDF dedans pour un traitement par lot [default: ""]
  -d, --dest[=DEST]      Répertoire de destination d'un traitement par lot exemple -d /path/to/jpg, ou format pour un pdf multipages, exemple -d path/to/pdf_%02d.jpg [default: ""]
  -c, --shave[=SHAVE]    Rogner en hauteur ou largeur avec -c XxY. (exemples : -c 40x40 ou bien -c x40, ou encore -c 40x) [default: "0"]
```

**spip conversion -h**

```
Usage:
  convertisseur:convertir [options]
  conversion

Options:
  -e, --extracteur[=EXTRACTEUR]  Type d'extracteur pour la conversion [default: ""] ; Extracteurs disponibles : docx, quark, html, spipins, quark_xml, indesign_xml, xml_ocr, xml_de, pmg, saveasxml
  -s, --source[=SOURCE]          Répertoire source de la collection à convertir, structuré en collection/numero/[fichiers] [default: "conversion_source"]
  -d, --dest[=DEST]              Répertoire de destination [default: "conversion_spip"]
  -b, --notes[=NOTES]            Convertir les notes de bas de pages (mettre -b oui) [default: ""]
```
  
**spip import -h**

```
Usage:
  convertisseur:importer [options]
  import

Options:
  -s, --source[=SOURCE]                              Répertoire source [default: "conversion_spip"]
  -d, --dest[=DEST]                                  id_rubrique de la rubrique où importer la hierarchie de rubriques et les articles défini dans les fichiers txt (en général l'id_secteur ou on veut importer) [default: "0"]
  -a, --auteur_defaut[=AUTEUR_DEFAUT]                Auteur par défaut (id_auteur) [default: "1"]
  -r, --racine_documents[=RACINE_DOCUMENTS]          path ajouté devant le `fichier` des documents joints importés [default: ""]
  -c, --conserver_id_article[=CONSERVER_ID_ARTICLE]  option -c oui pour conserver les id_article [default: ""]
```

**spip export -h**

```
Usage:
  convertisseur:exporter [options]
  export

Options:
  -s, --source[=SOURCE]    Table à exporter [default: "spip_articles"]
  -d, --dest[=DEST]        Répertoire où exporter au format texte [default: "spip_articles"]
  -b, --branche[=BRANCHE]  branche à exporter (id_secteur ou id_rubrique) [default: "0"]
  -t, --statuts[=STATUTS]  statuts des articles a exporter (séparé par une virgule) [default: "prop,prepa,publie"]
  -m, --modif[=MODIF]      date_modif après laquelle exporter [default: ""]
```
