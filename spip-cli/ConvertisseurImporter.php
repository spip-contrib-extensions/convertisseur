<?php

/***
 *
 * Importer en masse des fichiers txt dans spip_articles.
 * Les articles sont ajoutés avec leurs info sauf id_article dans des rubriques créées si besoin.
 * Les documents <docxxx> sont gérés
 * Les raccourcis de liens [xxx->12345] sont gérés
 */

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class ConvertisseurImporter extends Command {

	protected $id_rubriques_in = [];
	protected function configure() {
		$this
			->setName('convertisseur:importer')
			->setDescription('Importer des fichiers SPIP txt dans la table spip_articles (ou autre).')
			->setAliases([
				'import' // abbréviation commune pour "import"
			])
			->addOption(
				'source',
				's',
				InputOption::VALUE_OPTIONAL,
				'Répertoire source',
				'conversion_spip'
			)
			->addOption(
				'dest',
				'd',
				InputOption::VALUE_OPTIONAL,
				'id_rubrique de la rubrique où importer la hierarchie de rubriques et les articles défini dans les fichiers txt (en général l\'id_secteur ou on veut importer)',
				'0'
			)
			->addOption(
				'auteur_defaut',
				'a',
				InputOption::VALUE_OPTIONAL,
				'Auteur par défaut (id_auteur)',
				'1'
			)
			->addOption(
				'racine_documents',
				'r',
				InputOption::VALUE_OPTIONAL,
				'path ajouté devant le `fichier` des documents joints importés',
				''
			)
			->addOption(
				'conserver_id_article',
				'c',
				InputOption::VALUE_NONE,
				'option pour conserver les id_article',
				null
			)
			->addOption(
				'nb_max',
				'',
				InputOption::VALUE_OPTIONAL,
				'nombre maxi de fichiers à importer',
				null
			)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('iterateur/data');

		$dir_source = $input->getOption('source');
		$id_parent = $input->getOption('dest');
		$racine_documents = $input->getOption('racine_documents');
		$conserver_id_article = $input->getOption('conserver_id_article');
		$auteur_defaut = $input->getOption('auteur_defaut');
		$nb_max = $input->getOption('nb_max');

		$debug = $output->isDebug(); // option -vvv

		// Répertoire source
		if (!$dir_source || !is_dir($dir_source)) {
			$this->io->error("Préciser le répertoire avec les fichiers à importer. spip import --source='repertoire' ");
			return self::FAILURE;
		}
		if (!$id_parent) {
			$this->io->error("Indiquez un id_rubrique de destination avec l'option --dest");
			return self::FAILURE;
		}
		if (!$racine_documents) {
			$racine_documents = _DIR_IMG;
		}

		if (!function_exists('passthru')) {
			$this->io->error('<error>Votre installation de PHP doit pouvoir exécuter des commandes externes avec la fonction passthru().</error>');
			return self::FAILURE;
		}

		// Si c'est bon on continue
		include_spip('base/abstract_sql');

		// Champs d'un article
		$this->verifierStructureTableArticles();

		// on prend tous les fichiers txt dans la source, sauf si metadata.txt a la fin.
		$fichiers = $this->listerFichiersRepertoire($dir_source, "*.txt");
		// les fichiers .metadata.txt, on ignore
		$fichiers = array_filter($fichiers, function ($k) {return substr($k, -13) !== '.metadata.txt';}, ARRAY_FILTER_USE_KEY);
		if (empty($fichiers)) {
			$this->io->fail("Rien à importer dans $dir_source");
			// peut-etre il restait juste à corriger les liens internes...
			$this->corrigerLiensInternes($id_parent);
			return self::SUCCESS;
		}

		// start and displays the progress bar
		$progress = new ProgressBar($output, count($fichiers));
		$progress->setBarWidth(100);
		$progress->setRedrawFrequency(1);
		$progress->setMessage(" Import de $dir_source/*.txt en cours dans la rubrique $id_parent ... ", 'message'); /**/
		$progress->setMessage('', 'inforub');
		$progress->start();

		include_spip('inc/convertisseur');
		include_spip('action/corriger_liens_internes');
		include_spip("src/Texte/Collecteur/Liens");
		include_spip("src/Texte/Collecteur/Modeles");

		$cpt = 0;
		// si on a pas plus de fichier que le max a traiter, laisser le traitement aller au bout et corriger les liens internes
		if ($nb_max >= count($fichiers)) {
			$nb_max = null;
		}

		include_spip('inc/rubriques');
		$this->id_rubriques_in = calcul_branche_in($id_parent);
		$this->id_rubriques_in = explode(',', $this->id_rubriques_in);

		foreach ($fichiers as $fichier) {

			// date d'apres le nom du fichier
			if (preg_match('/^(\d{4})-\d{2}/', basename($fichier), $m)) {
				$mois = $m[0];
				$annee = $m[1];
			} else {
				$this->io->error("Fichier $fichier mal nommé");
				return self::FAILURE;
			}

			$data = $this->lireDonneesFichier($fichier);
			if ($debug) {
				$this->io->care("Donnees extraites du fichier $fichier :");
				$trace = json_decode(json_encode($data), true);
				$trace['texte'] = substr($trace['texte'], 0, 40) . " ...(". strlen($trace['texte']).")";
				$this->io->text(var_export($trace, true));
			}

			// dans quelle rubrique importer ?
			// La hierarchie est-elle précisée dans le fichier ? (en principe oui)
			$arbo = $this->creerArboRubriques($id_parent, $data['hierarchie'] ?: [$annee, "$annee-$mois"]);
			if ($debug) {
				$this->io->care("Arbo rubrique :");
				$this->io->text(var_export($arbo, true));
			}
			$id_rubrique = array_keys($arbo);
			$id_rubrique = end($id_rubrique);
			$hierarchie = implode('/', $arbo);

			$set = ['statut' => 'publie'];
			if (!empty($data['descriptif_rubrique']) or !empty($data['texte_rubrique'])) {
				$set['texte'] = $data['texte_rubrique'];
				$set['descriptif'] = $data['descriptif_rubrique'];
			}
			$up = sql_updateq('spip_rubriques', $set, 'id_rubrique=' . intval($id_rubrique));
			if ($debug) {
				$this->io->care("MAJ rubrique #$id_rubrique: $up");
				$this->io->text(var_export($set, true));
			}

			$rubrique = sql_fetsel('id_secteur,id_parent', 'spip_rubriques', "id_rubrique=" . intval($id_rubrique));
			if (empty($rubrique['id_secteur'])) {
				$this->io->error("Erreur de création de rubrique $id_rubrique : id_secteur=0");
				return self::FAILURE;
			}

			if ($up) {
				$progress->setMessage(" Rubrique $hierarchie => (" . $rubrique['id_secteur'] . ' > ' . $rubrique['id_parent'] . " > ) $id_rubrique ", 'inforub');
			}

			$progress->setMessage('', 'docs');
			$progress->setMessage('', 'mot');
			$progress->setMessage('', 'auteur');

			// inserer l'article

			// auteur par défaut (admin)
			if (!$auteur_defaut
				|| !($id_admin = sql_getfetsel('id_auteur', 'spip_auteurs', 'id_auteur=' . intval($auteur_defaut)))) {
				$id_admin = 12166;
			}

			$GLOBALS['auteur_session'] = ['id_auteur' => $id_admin];

			$options = [
				'id_rubriques_in' => $this->id_rubriques_in,
				'id_rubrique' => $id_rubrique,
				'fichier' => $fichier,
				'debug' =>$debug
			];
			if ($conserver_id_article && !empty($data['id_source'])) {
				$options['id_source'] = $data['id_source'];
			}
			if ($id_article = inserer_conversion($data['texte'], $options)) {
				// si option --conserver_id_article alors on renumerote selon le id_source (== id_article initial)
				// et on sait qu'il y a aucun lien a reecrire, c'est donc une option prioritaire
				$id_article_dest = null;
				if ($conserver_id_article) {
					$id_article_dest = $data['id_source'] ?? null;
				} elseif (!empty($data['id_article_dest'])) {
					$id_article_dest = $data['id_article_dest'];
				}

				if ($id_article_dest
					&& $id_article_dest != $id_article) {
					if ($debug) {
						$raison = (empty($data['id_article_dest']) ? "conserver id_article initial" : "matcher id_article_dest");
						$this->io->care("Renumeroter article pour $raison #$id_article_dest");
					}
					if (sql_countsel('spip_articles', 'id_article='.intval($id_article_dest))) {
						$this->io->error("Collision d'import article : il y a déjà un article existant #$id_article_dest, impossible de renumeroter #$id_article");
						$this->io->care("Suppression de l'article importé #$id_article");
						sql_delete('spip_articles', 'id_article='.intval($id_article));
						sql_delete('spip_auteurs_liens', "objet='article' and id_objet=" . intval($id_article));
						return self::FAILURE;
					}
					sql_update('spip_articles', ['id_article' => $id_article_dest], 'id_article=' . intval($id_article));
					// maj le lien auteur admin
					sql_update('spip_auteurs_liens', ['id_objet' => $id_article_dest], "objet='article' and id_objet=" . intval($id_article));
					$id_article = $id_article_dest;
				}

				// Créer les auteurs
				$id_auteurs = $this->ajouterAuteurs($progress, $id_article, $id_admin, $data['auteurs'], $debug);
				if ($debug) {
					$this->io->care($id_auteurs ? "Auteurs associés à l'article : #". implode(", #", $id_auteurs) : "Aucun auteur pour cet article");
				}

				// Créer des mots clés ?
				$id_mots = $this->ajouterMotsCles($progress, $id_article, $data['mots_cles'], $debug);
				if ($debug) {
					$this->io->care($id_mots ? "Mots associés à l'article : #". implode(", #", $id_mots) : "Aucun mot clé pour cet article");
				}


				$id_documents = $this->ajouterDocuments($progress, $id_article, $racine_documents, $data['documents'], $debug);
				if ($debug) {
					$this->io->care($id_documents ? "Documents associés à l'article : #". implode(", #", $id_documents) : "Aucun document pour cet article");
				}

				// recaler des liens [->123456] dans les textes
				// si on ne conserve pas le meme id_article

				// si pas option conserver_id_article il faut renumeroter tous les liens internes
				$collecteurLiens = new Spip\Texte\Collecteur\Liens();
				if (!$conserver_id_article && $collecteurLiens->detecter($data['texte'])) {
					if ($debug) {
						$this->io->care("Liens à corriger dans cet article, on ajoute à liens_a_corriger.txt");
					}
					file_put_contents("liens_a_corriger.txt", "$id_article	" . $data['id_source']."\n", FILE_APPEND);
				}

				$progress->setMessage("ajout de l'art $id_article", 'article');

				// on réindexe immédiatement avec avec le plugin indexer (Sphinx) le cas échéant.
				$progress->setMessage('', 'index');
				include_spip('indexer_pipelines');
				if (function_exists('indexer_redindex_objet')) {
					indexer_redindex_objet('article', $id_article, false);
					$progress->setMessage(" + indexation de $id_article", 'index');
				}

				// Si tout s'est bien passé, on avance la barre
				$progress->setMessage($fichier, 'filename');
				$progress->setFormat("<fg=white;bg=blue>%message% %article% %index%</>\n" . "<fg=white;bg=red>%inforub% %auteur% %mot%</>\n" . '%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%' . "\n  %filename%\n%docs%\n\n");
				$progress->advance();
				@rename($fichier, $fichier . '.imported');
			} else {
				$this->io->error("échec de l'import de $fichier : " . ($GLOBALS['log'] ?? 'erreur inconnue dans inserer_conversion()'));
				return self::FAILURE;
			}
			$cpt++;
			if (!is_null($nb_max) && $cpt >= $nb_max) {
				$this->io->care("il reste des fichiers à importer, on arrête là");
				return self::SUCCESS;
			}
		}

		// ensure that the progress bar is at 100%
		$progress->finish();

		if (!$this->corrigerLiensInternes($id_parent, $debug)) {
			return self::FAILURE;
		}

		$this->io->success("Import fini");

		return self::SUCCESS;
	}

	public function corrigerLiensInternes($id_parent, $debug = false) {
		if (file_exists('liens_a_corriger.txt')) {
			$this->io->section("Corriger les liens internes");
			// remapper les liens [->12345]
			lire_fichier('liens_a_corriger.txt', $articles);
			$corrections_liens = explode("\n", $articles);
			$corrections_liens = array_map('trim', $corrections_liens);
			$corrections_liens = array_filter($corrections_liens);

			if (is_array($corrections_liens)) {
				foreach ($corrections_liens as $v) {
					if ($v) {
						[$id_article, $id_source] = explode("\t", $v, 2);
						if ($debug) {
							$this->io->care("Corriger liens internes article #$id_article");
						}
						$res = convertisseur_corriger_liens_internes($id_article, $id_parent, ['chapo', 'texte']);
						if($res === null) {
							$this->io->error("Echec lecture de l'article #$id_article");
							return false;
						}
					}
				}
			}

			if (is_file('liens_a_corriger.txt')) {
				unlink('liens_a_corriger.txt');
			}
		}
		return true;
	}

	public function listerFichiersRepertoire($dir_import, $search = "*.txt") {
		$dir_import = rtrim($dir_import, '/') . '/';

		$fichiers = glob($dir_import . $search);

		$subdirs = glob($dir_import . '*', GLOB_ONLYDIR);
		foreach ($subdirs as $subdir) {
			foreach ($this->listerFichiersRepertoire($subdir, $search) as $f) {
				$fichiers[] = $f;
			}
		}
		return $fichiers;
	}

	protected function ajouterDocuments($progress, $id_article, $racine_documents, $documents, $debug = false) {
		static $champs_documents = null;
		if (is_null($champs_documents)) {
			$show = sql_showtable('spip_documents');
			$champs_documents = array_keys($show['field']);
		}

		$id_documents = [];
		// Créer des documents ?
		if (!empty($documents)) {
			// on commence par effacer les docs déjà sur l'article, puis on remet les mots.
			sql_delete('spip_documents_liens', 'id_objet = ' . intval($id_article) . ' and objet="article"');

			foreach ($documents as $doc) {
				$d = json_decode($doc, true);
				$id_doc = $d['id_document'] ?? 0;
				unset($d['id_document']);
				if (strlen($racine_documents) > 0 and !preg_match(',/$,', $racine_documents)) {
					$racine_documents = $racine_documents . '/';
				}

				if (!preg_match(',^https?://,', $d['fichier'])) {
					$d['fichier'] = $racine_documents . $d['fichier'];
				}

				// champs ok dans les documents ?
				foreach ($d as $k => $v) {
					if (in_array($k, $champs_documents)) {
						$document_a_inserer[$k] = $v;
					}
				}

				// insertion du doc
				$id_document = sql_getfetsel('id_document', 'spip_documents', 'fichier=' . sql_quote($d['fichier']));

				if (!$id_document) {
					$id_document = sql_insertq('spip_documents', $document_a_inserer);
					$progress->setMessage('Création du document ' . ($d['titre'] ?? '') . ' (' . $d['fichier'] . ')', 'docs');
					if ($debug) {
						$this->io->care("Ajout du document #$id_document " . json_encode($document_a_inserer));
					}
				} else {
					unset($document_a_inserer['id_document']);
					sql_updateq('spip_documents', $document_a_inserer, "id_document=" .intval($id_document));
				}

				if ($id_document and !sql_getfetsel('id_document', 'spip_documents_liens', "id_document=".intval($id_document)." and id_objet=".intval($id_article)." and objet='article'")) {
					sql_insertq('spip_documents_liens', [
						'id_document' => $id_document,
						'id_objet' => $id_article,
						'objet' => 'article',
						'vu' => 'oui'
					]);
				}

				$id_documents[] = $id_document;

				// modifier les champs texte qui utilisent peut etre un <doc123>
				if ($id_document && $id_doc) {
					$this->renumeroterModeles(
						$id_article,
						['doc', 'img', 'emb', 'image', 'video', 'audio'],
						$id_doc,
						$id_document,
						['texte', 'chapo', 'ps'],
						$debug
					);
				}
			}
		}
		return $id_documents;
	}

	protected function renumeroterModeles($id_article, $modeles_cherche, $id_cherche, $id_replace, $champs, $debug = false) {
		$article = sql_fetsel(implode(',', $champs), 'spip_articles', 'id_article=' . intval($id_article));
		if ($article) {
			$set = [];
			foreach ($article as $champ => $valeur) {
				$collecteurModeles = new Spip\Texte\Collecteur\Modeles();
				$modeles = $collecteurModeles->collecter($valeur);
				// commencer les remplacements par la fin sinon ca decalle les pos
				$modeles = array_reverse($modeles);
				foreach ($modeles as $modele) {
					if (in_array($modele['type'], $modeles_cherche) && $modele['id'] == $id_cherche) {
						if (!isset($set[$champ])) {
							$set[$champ] = $valeur;
						}

						$new = substr($modele['raw'], strlen("<" . $modele['type']));
						$new = ltrim($new);
						$new = substr($new, strlen($modele['id']));
						$new = "<" . $modele['type'] . $id_replace . $new;
						$set[$champ] = substr_replace($set[$champ], $new, $modele['pos'], $modele['length']);
						if ($debug) {
							$this->io->care("Remplacer modèle ".$modele['raw']." par $new...");
						}
					}
				}
			}
			if (!empty($set)) {
				sql_updateq('spip_articles', $set, 'id_article=' . intval($id_article));
				if ($debug) {
					$this->io->care("Mise à jour des modèles dans l'article $id_article");
				}

			}
		}
	}

	protected function ajouterMotsCles($progress, $id_article, $mots_cles, $debug = false) {

		$id_mots = [];
		// Créer des mots clés ?
		$mots_cles = array_filter($mots_cles);
		if (!empty($mots_cles)) {
			// on commence par effacer les mots déjà sur l'article, puis on remet les mots.
			sql_delete('spip_mots_liens', 'id_objet = ' . intval($id_article) . ' and objet="article"');

			foreach ($mots_cles as $mot) {
				// groupe mot-clé
				list($type_mot, $titre_mot) = explode('::', $mot);
				$type_mot = ($type_mot) ? $type_mot : 'Mots importés';

				$id_groupe_mot = sql_getfetsel('id_groupe', 'spip_groupes_mots', 'titre=' . sql_quote($type_mot));
				if (!$id_groupe_mot) {
					$id_groupe_mot = sql_insertq('spip_groupes_mots', ['titre' => $type_mot]);
				}

				$id_mot = sql_getfetsel('id_mot', 'spip_mots', 'titre=' . sql_quote($titre_mot));
				if (!$id_mot and $titre_mot != '') {
					$set = [
						'titre' => $titre_mot,
						'type' => $type_mot,
						'id_groupe' => $id_groupe_mot
					];
					$id_mot = sql_insertq('spip_mots', $set);
					if ($debug) {
						$this->io->care("Creation Mot clé #$id_mot " . json_encode($set));
					}
					$mot_m = substr('Création du mot ' . $titre_mot . ' (' . $type_mot . ')', 0, 100);
					$progress->setMessage($mot_m, 'mot');
				}

				if (!sql_getfetsel('id_mot', 'spip_mots_liens', "id_mot=".intval($id_mot)." and id_objet=".intval($id_article)." and objet='article'")) {
					sql_insertq('spip_mots_liens', [
						'id_mot' => $id_mot,
						'id_objet' => $id_article,
						'objet' => 'article'
					]);
				}
				$id_mots[] = $id_mot;
			}
		}
		return $id_mots;
	}

	protected function ajouterAuteurs($progress, $id_article, $id_admin, $auteurs, $debug = false) {
		// Créer l'auteur ?
		$id_auteurs = [];
		if (!empty($auteurs)) {
			// on efface les auteurs, puis on remet les nouveaux
			sql_delete('spip_auteurs_liens', 'id_objet = ' . intval($id_article) . ' AND objet="article" AND id_auteur !=' . intval($id_admin));
			$id_auteurs = [];

			foreach ($auteurs as $auteur) {
				$parts = explode('::', $auteur, 2);
				$nom_auteur = array_shift($parts);
				$bio_auteur = array_shift($parts) ?? '';
				// On essaie de trouver un nom*prénom dans les auteurs
				$a_nom = explode(' ', $nom_auteur);
				$prenom_nom = array_pop($a_nom) . '*' . join(' ', $a_nom);

				// echo "\n$prenom_nom\n" ;

				if (!$id_auteur = sql_getfetsel('id_auteur', 'spip_auteurs', 'nom=' . sql_quote($prenom_nom))) {
					$id_auteur = sql_getfetsel('id_auteur', 'spip_auteurs', 'nom=' . sql_quote($nom_auteur));
				}

				if (!$id_auteur) {
					$set = [
						'nom' => $nom_auteur,
						'statut' => '1comite',
						'bio' => $bio_auteur
					];
					$id_auteur = sql_insertq('spip_auteurs', $set);
					if ($debug) {
						$this->io->care("Creation Auteur #$id_auteur " . json_encode($set));
					}
					$auteur_m = substr("Création de l'auteur " . $auteur, 0, 100);
					$progress->setMessage($auteur_m, 'auteur');
				}

				if (!sql_getfetsel('id_auteur', 'spip_auteurs_liens', "id_auteur=".intval($id_auteur)." and id_objet=".intval($id_article)." and objet='article'")) {
					sql_insertq('spip_auteurs_liens', [
						'id_auteur' => $id_auteur,
						'id_objet' => $id_article,
						'objet' => 'article'
					]);
				}
				$id_auteurs[] = $id_auteur;
			}
		}
		return $id_auteurs;
	}

	protected function creerArboRubriques($id_parent, $arbo) {
		include_spip('inc/rubriques');

		$hierarchie = [];

		$id_parent_rubrique = $id_parent;
		$id_secteur = 0;
		$lang = $GLOBALS['meta']['langue_site'];
		if ($id_parent_rubrique > 0) {
			$rubrique = sql_fetsel('id_secteur,lang','spip_rubriques',"id_rubrique=".intval($id_parent_rubrique));
			$id_secteur = $rubrique['id_secteur'];
			$lang = $rubrique['lang'];
		}

		foreach ($arbo as $titre) {
			// retablir les </multi> et autres balises fermantes html
			$titre = preg_replace(',<@([a-z][^>]*)>,ims', "</\\1>", $titre);
			// Les éventuels / sont échappés \/ ; exemple : 1999/1999\/06
			$titre = preg_replace('`\\\/`', '/', $titre);

			$id_rubrique = sql_getfetsel(
				'id_rubrique',
				'spip_rubriques',
				'titre = ' . sql_quote($titre) . ' AND id_parent=' . intval($id_parent_rubrique)
			);

			if (!$id_rubrique) {
				$id_rubrique = sql_insertq('spip_rubriques', [
					'titre' => $titre,
					'id_parent' => $id_parent_rubrique,
					'statut' => 'prepa',
					'id_secteur' => $id_secteur,
					'lang' => $lang
				]);
				if (!$id_secteur) {
					$id_secteur = $id_rubrique;
					sql_updateq(
						'spip_rubriques',
						['id_secteur' => $id_secteur],
						'id_rubrique=' . intval($id_rubrique)
					);
				}
				$this->id_rubriques_in[] = $id_rubrique;
			}
			$hierarchie[$id_rubrique] = $titre;
			// pour la recursion
			$id_parent_rubrique = $id_rubrique;
		}

		return $hierarchie;
	}

	protected function lireDonneesFichier($fichier) {
		// Si des <ins> correspondent à des champs metadonnees connus, on les collecte.
		$champs_metadonnees = ['mots_cles', 'auteurs', 'hierarchie', 'documents', 'descriptif_rubrique', 'texte_rubrique'];
		$data = [
			'mots_cles' => [],
			'auteurs' => [],
			'hierarchie' => [],
			'documents' => [],
			'descriptif_rubrique' => '',
			'texte_rubrique' => '',
			'id_article_dest' => 0,
			'id_source' => 0,
		];

		// chopper l'id_parent dans le fichier ?
		$content = file_get_contents($fichier);

		// menage
		//@@COLLECTION:esRetour ligne automatique
		//@@SOURCE:article914237.html

		if (str_contains($content, '@@COLLECTION')) {
			$content = preg_replace('/@@COLLECTION.*/', '', $content);
		}
		if (str_contains($content, '@@SOURCE')) {
			$content = preg_replace('/@@SOURCE.*/', '', $content);
		}

		if (preg_match_all(",<ins[^>]+class='(.*?)'[^>]*?>(.*?)</ins>,ims", $content, $matches, PREG_SET_ORDER)) {
			foreach ($matches as $m) {
				$class = $m[1];
				// class="truc" => $data['truc']
				if (isset($data[$class])) {
					if (strlen(trim($m[2]))) {
						$data[$class] = is_array($data[$class]) ? explode('@@', $m[2]) : $m[2];
					}
					// et enlever du content
					$content = substr_replace($content, '', strpos($content, $m[0]), strlen($m[0]));
				}
			}
		}

		if (preg_match(",<ins class='id_article'>(.*?)</ins>,ims", $content, $matches)) {
			$data['id_source'] = intval($matches[1]);
		}

		// tout le reste vans dans 'texte'
		$data['texte'] = $content;
		return $data;
	}

	protected function verifierStructureTableArticles() {
		$show = sql_showtable('spip_articles');

		// Ajout d'un champ la premiere fois pour stocker les éventuelles <ins> qui n'ont pas de champs (peut-être long).
		if (!isset($show['field']['metadonnees'])) {
			$this->io->care("MAJ BDD : alter table spip_articles add metadonnees MEDIUMTEXT NOT NULL DEFAULT ''");
			sql_alter("TABLE spip_articles ADD metadonnees MEDIUMTEXT NOT NULL DEFAULT ''");
		}
		// Ajout d'un champ la premiere fois pour stocker l'id_article original (pour ensuite remapper les liens [->123]).
		if (!isset($show['field']['id_source'])) {
			$this->io->care('MAJ BDD : alter table spip_articles add id_source BIGINT(21)');
			sql_alter('TABLE spip_articles ADD id_source BIGINT(21)');
		}
		// Ajout d'un champ la premiere fois pour stocker le nom du fichier source, pour reconnaitre un article déjà importé.
		if (!isset($show['field']['fichier_source'])) {
			$this->io->care("MAJ BDD : alter table spip_articles add fichier_source MEDIUMTEXT NOT NULL DEFAULT ''");
			sql_alter("TABLE spip_articles ADD fichier_source MEDIUMTEXT NOT NULL DEFAULT ''");
		}
	}
}
