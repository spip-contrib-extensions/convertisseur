<?php

/***
 *
 * Installer ImageMagick pour que cela fonctionne. (brew install imagemagick sous Mac)
 */

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConvertisseurImagesOptimiser extends Command {
	protected function configure() {
		$this
			->setName('convertisseur:images:optimiser')
			->setDescription('Compression et/ou redimensionnement d\'une ou plusieurs images.')
			->setAliases([
				'optimiser_images', //historique
				'optimg', // abbréviation pas ouf pour ca
			])
			->addArgument(
				'image',
				InputArgument::OPTIONAL,
				'Image à optimiser.'
			)
			->addOption(
				'source',
				's',
				InputOption::VALUE_OPTIONAL,
				'Répertoire source. Optimiser toutes les images d\'un répertoire. Exemple : `spip optimg -s IMG`.',
				'0'
			)
			->addOption(
				'dest',
				'd',
				InputOption::VALUE_OPTIONAL,
				'Répertoire de destination. Exemple : `spip optimg -d mon_repertoire mon_image.jpg`. Si ce répertoire n\'est pas précisé, on écrase l\'image avec sa version optimisée.',
				'0'
			)
			->addOption(
				'resize',
				'r',
				InputOption::VALUE_OPTIONAL,
				'Redimensionner la largeur à n px, en conservant les proportions pour calculer la hauteur. Exemple : `spip optimg -r 900 mon_image.jpg`',
				'0'
			)
			->addOption(
				'compression',
				'c',
				InputOption::VALUE_OPTIONAL,
				'Compresser les images à 80%. Exemple : `spip optimg -c 80 mon_image.jpg`',
				'0'
			)
			->addOption(
				'border',
				'b',
				InputOption::VALUE_OPTIONAL,
				'Ajouter une bordure noire. Exemple : `spip optimg -b 2x2 mon_image.jpg`',
				'0'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output) {

		include_spip('iterateur/data');

		$source = $input->getOption('source');
		$dest = $input->getOption('dest');
		$resize = $input->getOption('resize');
		$compression = $input->getOption('compression');
		$border = $input->getOption('border');
		$image = $input->getArgument('image');

		chdir(_ROOT_RACINE);

		if (!function_exists('passthru')) {
			$this->io->error('Votre installation de PHP doit pouvoir exécuter des commandes externes avec la fonction passthru().');
			return self::FAILURE;
		}
		if (!$image and !$source) {
			$this->io->error('Indiquez une image ou un dossier source à convertir');
			return self::FAILURE;
		}

		// Si c'est bon on continue
		$param_d = $param_r = $param_c = $param_b = '';
		$label_d = $label_r = $label_c = $label_b = '';

		if (strlen($dest) > 1) {
			$dest = rtrim($dest, '/') . '/';
			$label_d = " dans $dest";
			if (!is_dir($dest)) {
				mkdir($dest);
			}
			$param_d = $dest;
		}
		if ($resize > 0) {
			$label_r = " en redimensionnant la largeur à $resize px ";
			$param_r = $resize;
		}

		if ($compression > 0) {
			$label_c = " en compressant à $compression % ";
			$param_c = $compression;
		}

		if ($border != 0) {
			$label_b = " avec une bordure noire de $border px";
			$param_b = $border;
		}

		// optimisation imagemagick
		if ($image) {
			$fichiers_jpg = [$image];
		} else {
			$source = rtrim($source, '/') . '/';
			$fichiers_jpg = $source ? preg_files($source, '\.(jpg|tif)$') : [];
		}

		$nb_fichiers = count($fichiers_jpg);
		if (!$nb_fichiers) {
			$this->io->care("Rien à faire, aucune image jpg|tif dans $source");
			return self::FAILURE;
		}

		if ($nb_fichiers > 1) {
			$output->writeln("<info>$nb_fichiers Images à convertir dans $source/</info>");
		}
		$output->writeln("<info>C'est parti pour une petite optimisation d'image ${label_r}${label_d}${label_b}${label_c} !</info>");

		foreach ($fichiers_jpg as $fichier_jpg) {
			// si c'est un traitement par lot, calculer param_d pour chaque fichier
			if (!$image) {
				if ($dest) {
					$path = dirname($fichier_jpg) . '/';
					if (str_contains($path, $source)) {
						$path = explode($source, $path);
						$path = end($path);
					}
					$path = ltrim($path, '/');
					$param_d = self::recursMkdir($dest, $path);
				}
				else {
					$param_d = dirname($fichier_jpg) . '/';
				}
			}

			$fichier_dest = $param_d . basename($fichier_jpg);

			$this->io->care($fichier_jpg);
			$result_code = self::optImg($output, $fichier_jpg, $fichier_dest, $param_r, $param_c, $param_b);
			if ($result_code) {
				$this->io->error("Echec de la conversion code $result_code");
				return self::FAILURE;
			}
		}

		return self::SUCCESS;
	}

	protected static function optImg(OutputInterface $output, $fichier_source, $fichier_dest, $resize = 0, $compress = 0, $border = '') {

		$resize = intval($resize);
		$compress = intval($compress);
		if (!preg_match('`\d+x\d+`', $border)) {
			$border = '' ;
		}

		$args = [
			($resize ? "-resize {$resize}x" : ''),
			'-strip',
			($compress ? "-gaussian-blur 0.05 -quality ${compress}%" : ''),
			($border ? "-bordercolor black -border ${border}" : ''), // 2x2 pour deux px
			escapeshellarg($fichier_source),
			escapeshellarg($fichier_dest),
		];

		$before = @filemtime($fichier_dest);

		// executer la commande : on delegue a une fonction surchargeable
		$exec_optimg = charger_fonction('exec_optimg', 'convertisseur');
		$result_code = $exec_optimg($fichier_source, $fichier_dest, $resize, $compress, $border);

		// si pas d'erreur, verifier que le fichier attendu a bien été généré
		if (!$result_code) {
			clearstatcache();
			if (!file_exists($fichier_dest) or !filesize($fichier_dest) or filemtime($fichier_dest) === $before) {
				$output->writeln("<error>Echec génération fichier $fichier_dest</error>");
				$result_code = 127;
			}
		}

		return $result_code;
	}

	protected static function recursMkdir($base, $dir) {

		$current = rtrim($base, '/') . '/';
		if ($dir) {
			$parts = explode('/', $dir);
			if (reset($parts) !== '') {
				do {
					$current .= array_shift($parts) . '/';
					if (!is_dir($current)) {
						mkdir($current);
					}
				} while (count($parts));
			}
		}
		return $current;
	}
}
