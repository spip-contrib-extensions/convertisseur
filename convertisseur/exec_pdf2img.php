<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function convertisseur_exec_pdf2img_dist($fichier_pdf, $fichier_dest, $shave) {

	# une seule page
	# echo "convert -verbose -colorspace sRGB -resize 1500 -interlace none -density 300 -background white -alpha remove -quality 80 -shave $shave $pdf $fichier_dest"
	$args = [
		'-verbose',
		'-colorspace sRGB',
		'-resize 1500',
		'-interlace none',
		'-density 300',
		'-background white',
		'-quality 80',
		'-alpha remove',
		($shave ? '-shave ' . escapeshellarg($shave) : ''),
		escapeshellarg($fichier_pdf),
		escapeshellarg($fichier_dest),
	];

	$command = 'convert ' . implode(' ', $args);
	if (_IS_CLI) {
		echo "$command\n";
		passthru("$command 2>&1", $result_code);
	}
	else {
		$output = [];
		exec("$command 2>&1", $output, $result_code);
		spip_log("$command\n" . implode("\n", $output), 'convertisseur' . _LOG_DEBUG);
	}

	// si erreur, verifier les binaires
	if ($result_code) {
		include_spip('inc/convertisseur');
		convertisseur_tester_binaire('convert');
		convertisseur_tester_binaire('gs');
	}

	return $result_code;
}
