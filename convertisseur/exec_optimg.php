<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

function convertisseur_exec_optimg_dist($fichier_source, $fichier_dest, $resize, $compress, $border) {

	$args = [
		($resize ? "-resize {$resize}x" : ''),
		'-strip',
		'-interlace Plane',
		($compress ? "-gaussian-blur 0.05 -quality ${compress}%" : ''),
		($border ? "-bordercolor black -border ${border}" : ''), // 2x2 pour deux px
		escapeshellarg($fichier_source),
		escapeshellarg($fichier_dest),
	];

	$command = 'convert ' . implode(' ', $args);
	if (_IS_CLI) {
		echo "$command\n";
		passthru("$command 2>&1", $result_code);
	}
	else {
		$output = [];
		exec("$command 2>&1", $output, $result_code);
		spip_log("$command\n" . implode("\n", $output), 'convertisseur' . _LOG_DEBUG);
	}

	// si erreur, verifier le binaire
	if ($result_code) {
		include_spip('inc/convertisseur');
		convertisseur_tester_binaire('convert');
	}

	return $result_code;
}
