<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-convertisseur
// Langue: fr
// Date: 04-02-2012 18:31:53
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'convertisseur_description' => 'Convertit les formats de type wiki, phpBB, quark xpress tags en format SPIP et vice versa ; intègre au besoin les textes convertis dans des articles ; on peut uploader plusieurs textes en un seul coup en les zippant au préalable',
	'convertisseur_nom' => 'Convertisseur de formats',
	'convertisseur_slogan' => 'Convertir les textes wiki, phpBB et quark xpress en SPIP',
);
?>